import psycopg2

from model.game import Game


# https://www.geeksforgeeks.org/postgresql-insert-data-into-a-table-using-python/?ref=ml_lbp


def connect(db_name):
    try:
        conn = psycopg2.connect(
            database=db_name,
            user='postgres',
            password='postgres',
            host='localhost',
            port='5432'
        )
        conn.autocommit = True
        return conn
    except Exception as e:
        print(e)
        return None

def cread_db(db_name="direction"):
    # connection establishment
    conn = connect("postgres")
    if conn is None:
        return

    # Creating a cursor object
    cursor = conn.cursor()

    try:
        cursor.execute( f"""DROP DATABASE {db_name};""")
    except Exception as e:
        print(e)

    # query to create a database
    sql = f"""CREATE database {db_name}"""
    # executing above query
    cursor.execute(sql)
    print("Database has been created successfully !!")
    cursor.close()
    conn.close()


def create_tables(db_name="direction"):
    try:
        sql_cmd = open("create_tables.sql", "r").read()
    except Exception as e:
        print(e)
        return

    conn = connect(db_name)
    if conn is None:
        return

    with conn.cursor() as cursor:
        try:
            cursor.execute(sql_cmd)

            # a more robust way of handling errors
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)


def fill_pawn_table(db_name="direction"):
    conn = connect(db_name)
    if conn is None:
        return

    with conn.cursor() as cursor:
        try:
            for kind in ["King", "Lady", "Mad", "Horse", "Tower", "P0", "P1"]:
                for owner in ["black", "white"]:
                    for position in range(8 * 8):  # FIXME
                        position_1d = position + int(position / 8) + 1
                        for rotation in range(8):
                            sql_cmd = f"""
                            INSERT INTO pawn (pawn_owner, kind, position_1d, rotation)
                            VALUES ('{owner}', '{kind}', {position_1d}, {rotation});
                            """
                            cursor.execute(sql_cmd)
                    #sql_cmd = f"""
                    #INSERT INTO pawn (pawn_owner, kind, alive)
                    #VALUES ('{owner}', '{kind}', false);
                    #"""
                    #cursor.execute(sql_cmd)

            print(f"{7 * 2 * 64 * 8 + 7 * 2} cases added")

            # a more robust way of handling errors
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)


def insert_game(game: Game, db_name="direction"):
    conn = connect(db_name)
    if conn is None:
        return

    map_kind = {"Ki": "King", "La": "Lady", "Ma": "Mad", "Ho": "Horse", "To": "Tower", "P0": "P0", "P1": "P1"}
    with conn.cursor() as cursor:
        try:
            for i in range(6):
                str_state = game.historic[i + 1]
                squares = str_state.split("|")[1:]
                position = 0
                for square in squares:
                    if len(square) == 5 and square != "     ":
                        color = "black" if square[0] == "b" else "white"
                        kind = map_kind.get(square[1:3], "")
                        rotation = int(square[4])
                        pawn_id = select_pawn(color, kind, position, rotation, db_name)
                        # TODO
                    position += 1

            # a more robust way of handling errors
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)


def select_pawn(pawn_owner, kind, position, rotation, db_name="direction"):
    conn = connect(db_name)
    if conn is None:
        return

    with conn.cursor() as cursor:
        try:
            # simple single row system query
            cursor.execute(f"SELECT pawn_id FROM pawn WHERE pawn_owner='{pawn_owner}' AND kind='{kind}' AND position_1d={position} AND rotation={rotation}")

            # returns a single row as a tuple
            single_row = cursor.fetchone()
            if single_row is not None:
                return single_row[0]

            # a more robust way of handling errors
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)


def select(table, columns, db_name="direction"):
    # TODO https://wiki.postgresql.org/wiki/Psycopg2_Tutorial
    pass


if __name__ == '__main__':
    cread_db()
    create_tables()
    fill_pawn_table()
    print(select_pawn("white", "King", 1, 0))
