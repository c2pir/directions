import numpy as np


class Gene:
    def __init__(self, name, domain: list):
        """
        :param name:
        :param domain: list of possible values
        """
        self.name = name
        self.domain = domain
        self.value = None
        self.alea()

    def duplicate(self):
        """Duplicate a gene (does not copy the value).
        :return: a new  gene
        """
        return Gene(self.name, self.domain)

    def alea(self):
        """Set the gene value randomly
        :return: None
        """
        index = np.random.randint(0, len(self.domain))
        self.value = self.domain[index]

    def __str__(self):
        return f"{self.name}: {self.value}"


class ADN:
    MUTATION = 0.02  # taux de mutations

    def __init__(self):
        self.genes = []

    def add(self, name, range: list):
        """Add a gene.
        :param name: gene unique name
        :param range: gene range
        :return: None
        """
        self.genes.append(Gene(name, range))

    def __getitem__(self, item):
        for gene in self.genes:
            if gene.name == item:
                return gene.value
        return None

    def __setitem__(self, key, value):
        for gene in self.genes:
            if gene.name == key:
                gene.value = value
                return

    def __add__(self, other):
        res = ADN()
        for i in range(len(self.genes)):
            if np.random.random() < self.MUTATION:
                res.genes.append(self.genes[i].duplicate())
            else:
                if np.random.randint(0, 2) == 0:
                    res.genes.append(self.genes[i])
                else:
                    res.genes.append(other.genes[i])
        return res

    def __sub__(self, other):
        """Quantify ADN differences.
        :param other: other ADN
        :return: mean relative gap
        """
        delta = 0
        for gene in self.genes:
            v = other[gene.name]
            assert (v is not None)
            if (gene.value + v) != 0:
                delta += abs(gene.value - v) / (gene.value + v)
        return delta / len(self.genes)

    def __str__(self):
        msg = ""
        for gene in self.genes:
            msg += str(gene) + "\n"
        return msg

    def get_domain(self, name):
        for gene in self.genes:
            if gene.name == name:
                return gene.domain
        return None

