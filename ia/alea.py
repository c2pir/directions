import logging

import numpy as np
from ia.base import Brain


class God(Brain):
    def __init__(self, owner=None):
        Brain.__init__(self, owner)

    def evaluate(self, game):
        return np.random.random()


if __name__ == '__main__':
    from model.game import Game

    logging.basicConfig(format="%(levelname)s:%(filename)s:%(lineno)d %(message)s")
    loggerB = logging.getLogger("Brain")
    loggerB.setLevel(logging.DEBUG)

    game = Game()
    game.new()

    brain = God(game.players[0])
    best_action = brain.best_action(game)
