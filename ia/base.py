import logging
import numpy as np

from model.decorators import timestamp
from model.game import Game

loggerB = logging.getLogger("Brain")


class Brain:
    """
    Virtual class for IA
    """

    def __init__(self, owner=None):
        self.owner = owner
        self.memory = []

    @timestamp("Brain")
    def best_action(self, game, minimum=float('-inf')):
        """Find the best action in the available ones.

        Args:
            minimum: the absolute minimum game evaluation possible
            game: the game object describing the actual situation

        Returns: best action to do.
        """
        res = None
        best_notation = minimum
        cpt_sames = 1

        legal_actions = [*game.available_actions]
        for action in legal_actions:

            # do an available action
            game.push(action)
            # assert game.board.check()

            # ignore actions that leave the IA in checkmate
            if game.checkmate(self.owner):
                notation = minimum

            # if action make the win, this is the best move
            elif not game.actual_player.is_alive:
                notation = float('inf')

            # compute evaluation
            else:
                notation = self.evaluate(game)
            loggerB.debug(f"{action}, notation: {notation}")

            # TODO avoid repetition if possible

            # undo action
            game.pop()
            # assert game.board.check()

            # save it if it is the actual best
            if notation > best_notation:
                res = action
                best_notation = notation
                cpt_sames = 1

            # random choice if notations are the same for several actions
            elif (notation == best_notation) and (notation != minimum):
                cpt_sames += 1  # for quasi same probabilities
                if np.random.random() < 1.0 / cpt_sames:
                    res = action

        loggerB.debug(f"Choice: {res}, cpt_sames: {cpt_sames}")
        return res

    def evaluate(self, game: Game) -> float:
        """Method used to give a notation for a given game situation.

        Args:
            game: the game object

        Returns: a notation
        """
        raise NotImplementedError


