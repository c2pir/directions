# Importez la bibliothèque pour générer les mouvements possibles dans le jeu d'échecs
import chess
import chess.svg
import chess.pgn


# Définissez la fonction d'évaluation pour la position du plateau
def evaluate_board(board):
    # Cette fonction pourrait être améliorée en fonction de la complexité de l'évaluation
    return board.evaluate()


# Implémentez la fonction minimax avec élagage alpha-bêta
def minimax(board, depth, maximizing_player, alpha, beta):
    if depth == 0 or board.is_game_over():
        return evaluate_board(board)

    legal_moves = list(board.legal_moves)

    if maximizing_player:
        max_eval = float('-inf')
        for move in legal_moves:
            board.push(move)
            eval = minimax(board, depth - 1, False, alpha, beta)
            board.pop()
            max_eval = max(max_eval, eval)
            alpha = max(alpha, eval)
            if beta <= alpha:
                break
        return max_eval
    else:
        min_eval = float('inf')
        for move in legal_moves:
            board.push(move)
            eval = minimax(board, depth - 1, True, alpha, beta)
            board.pop()
            min_eval = min(min_eval, eval)
            beta = min(beta, eval)
            if beta <= alpha:
                break
        return min_eval


# Implémentez la fonction pour choisir le meilleur coup
def find_best_move(board, depth):
    legal_moves = list(board.legal_moves)
    best_move = None
    best_eval = float('-inf')

    for move in legal_moves:
        board.push(move)
        eval = minimax(board, depth - 1, False, float('-inf'), float('inf'))
        board.pop()

        if eval > best_eval:
            best_eval = eval
            best_move = move

    return best_move


# Exemple d'utilisation
if __name__ == "__main__":
    board = chess.Board()

    # Effectuez quelques mouvements pour illustrer l'utilisation
    moves = ["e4", "e5", "Nf3", "Nc6", "Bb5", "a6"]
    for move in moves:
        board.push_uci(move)

    # Trouvez le meilleur coup pour les Blancs avec une profondeur de recherche de 3
    best_move = find_best_move(board, depth=3)

    print("Meilleur coup:", best_move.uci())

    # Vous pouvez également afficher le plateau avec la bibliothèque chess.svg
    print(board)
