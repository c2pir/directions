import os
from ia.base import Brain
from ia.adn import ADN


class IAGen(Brain):
    def __init__(self, owner=None):
        Brain.__init__(self, owner)
        self.adn = ADN()

        ## SELF (positive)
        # pawn safety = len(protected) - len(threatened)
        self.adn.add("self/safety", [0, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 10])
        self.adn.add("self/Lady/safety", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/Mad/safety", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/Horseman/safety", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/Tower/safety", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/P0/safety", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/P1/safety", [i / 20.0 for i in range(20 + 1)])

        # what I threaten
        self.adn.add("self/threatened", [0, 0.1, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 10])
        self.adn.add("self/King/threatened", [999])
        self.adn.add("self/Lady/threatened", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/Mad/threatened", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/Horseman/threatened", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/Tower/threatened", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/P0/threatened", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/P1/threatened", [i / 20.0 for i in range(20 + 1)])

        # what is protected by me
        self.adn.add("self/protected", [0, 0.1, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 10])
        self.adn.add("self/Lady/protected", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/Mad/protected", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/Horseman/protected", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/Tower/protected", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/P0/protected", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("self/P1/protected", [i / 20.0 for i in range(20 + 1)])

        # total number of translations
        self.adn.add("self/translations", [0, 0.1, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 10])
        self.adn.add("self/King/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("self/Lady/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("self/Mad/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("self/Horseman/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("self/Tower/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("self/P0/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("self/P1/translations", [i / 10.0 for i in range(10 + 1)])

        ## OTHER (negative)
        # is still alive
        self.adn.add("other/base", [0, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 10])
        self.adn.add("other/Lady/base", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Mad/base", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Horseman/base", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Tower/base", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/P0/base", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/P1/base", [i / 20.0 for i in range(20 + 1)])

        # pawn safety = len(protected) - len(threatened)
        self.adn.add("other/safety", [0, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 10])
        self.adn.add("other/Lady/safety", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Mad/safety", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Horseman/safety", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Tower/safety", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/P0/safety", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/P1/safety", [i / 20.0 for i in range(20 + 1)])

        # mine menaced by the other
        self.adn.add("other/threatened", [0, 0.1, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 10])
        self.adn.add("other/King/threatened", [1000])
        self.adn.add("other/Lady/threatened", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Mad/threatened", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Horseman/threatened", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Tower/threatened", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/P0/threatened", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/P1/threatened", [i / 20.0 for i in range(20 + 1)])

        # ce qui est protégé par l'autre
        self.adn.add("other/protected", [0, 0.1, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 10])
        self.adn.add("other/Lady/protected", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Mad/protected", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Horseman/protected", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/Tower/protected", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/P0/protected", [i / 20.0 for i in range(20 + 1)])
        self.adn.add("other/P1/protected", [i / 20.0 for i in range(20 + 1)])

        # total number of translations
        self.adn.add("other/translations", [0, 0.1, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 10])
        self.adn.add("other/King/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("other/Lady/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("other/Mad/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("other/Horseman/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("other/Tower/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("other/P0/translations", [i / 10.0 for i in range(10 + 1)])
        self.adn.add("other/P1/translations", [i / 10.0 for i in range(10 + 1)])

    def load(self, filename):
        """Load ADN file

        Args:
            filename:

        Returns: None
        """
        filepath = filename
        if not os.path.isfile(filename):
            filepath = os.path.dirname(__file__) + "/" + filename 
        
        f = open(filepath, "r")
        txt = f.read()
        f.close()
        for line in txt.split("\n"):
            tmp = line.split(": ")
            if len(tmp) != 2:
                continue
            self.adn[tmp[0]] = eval(tmp[1])

    def save(self, filename):
        filepath = filename
        if not os.path.isfile(filename):
            filepath = os.path.dirname(__file__) + "/" + filename

        f = open(filepath, "w")
        f.write(str(self.adn))
        f.close()

    def __add__(self, other):
        res = IAGen(self.owner)
        res.adn = self.adn + other.adn
        return res

    def evaluate(self, game):
        res = 0
        for pawn in game.board.pawns:
            what = pawn.__class__.__name__
            safety = len(pawn.protected) - len(pawn.threatened)

            if pawn.owner == self.owner:
                who = "self"

                # safety
                if what != "King":
                    res += self.adn[f"{who}/safety"] * self.adn[f"{who}/{pawn.__class__.__name__}/safety"] * safety

                # Threatened notation
                for p in pawn.threatens:
                    res += self.adn[f"{who}/threatened"] * self.adn[f"{who}/{p.__class__.__name__}/threatened"]

                # Protected notation
                # The king can't be protected from direct threats
                for p in pawn.protects:
                    res += self.adn[f"{who}/protected"] * self.adn[f"{who}/{p.__class__.__name__}/protected"]

                res += (self.adn[f"{who}/translations"] * self.adn[f"{who}/{pawn.__class__.__name__}/translations"] 
                        * pawn.translations_count)
            else:
                who = "other"

                if what != "King":
                    # safety
                    res -= self.adn[f"{who}/safety"] * self.adn[f"{who}/{pawn.__class__.__name__}/safety"] * safety

                    # base notation (pawn still alive indicator)
                    res -= self.adn[f"{who}/base"] * self.adn[f"{who}/{what}/base"]

                # Threatened notation
                for p in pawn.threatens:
                    if p.__class__.__name__ != "King":
                        res -= self.adn[f"{who}/threatened"] * self.adn[f"{who}/{p.__class__.__name__}/threatened"]

                # Protected notation
                # The king can't be protected from direct threats
                for p in pawn.protects:
                    res -= self.adn[f"{who}/protected"] * self.adn[f"{who}/{p.__class__.__name__}/protected"]

                res -= (self.adn[f"{who}/translations"] * self.adn[f"{who}/{pawn.__class__.__name__}/translations"] 
                        * pawn.translations_count)

        return int(res)
