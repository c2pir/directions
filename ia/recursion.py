from ia.base import Brain
from model.game import Indicators


class IADepth(Brain):
    def __init__(self, depth, owner=None, alpha=1.0):
        Brain.__init__(self, owner)
        self.alpha = alpha
        self.max_depth = depth

    def evaluate(self, game):
        return self.minimax(game, self.max_depth, False, float('-inf'), float('inf'))

    def minimax(self, game, depth, maximizing_player, alpha, beta):
        """
        Args:
            game: game object
            depth: remaining depths
            maximizing_player: True if it is this IA turn
            alpha: maximum score (for this ia) at this ia turn
            beta: minimum score (for this ia) at opponent turn

        Returns: evaluation
        """
        if depth == 0 or game.over:
            return self.notation(game)

        legal_actions = [*game.available_actions]

        if maximizing_player:
            max_eval = float('-inf')
            for action in legal_actions:
                game.push(action)
                eval = self.minimax(game, depth - 1, False, alpha, beta)
                game.pop()
                max_eval = max(max_eval, eval)
                alpha = max(alpha, eval)
                if beta <= alpha:
                    break
            return max_eval

        else:
            min_eval = float('inf')
            for action in legal_actions:
                game.push(action)
                eval = self.minimax(game, depth - 1, True, alpha, beta)
                game.pop()
                min_eval = min(min_eval, eval)
                beta = min(beta, eval)
                if beta <= alpha:
                    break
            return min_eval

    def notation(self, game):
        """Compute a game situation evaluation (the greater it is the better it is for the owner).

        Args:
            game: the given game situation

        Returns: game state notation
        """
        res = game.indicators.notation_2(self.owner, alpha=self.alpha)
        return res
