"""
All available in game actions.
"""
import logging
from model.pawns import King

logger = logging.getLogger("Action")


class Action:
    """Mother class for all possible actions."""
    def __init__(self, who):
        self.pawn = who

    def do(self, board):
        """Do this action on the given board.

        Args:
            board: Board object

        Returns: list of dones actions
        """
        raise NotImplementedError

    def undo(self, board):
        """Undo this action on the given board.

        Args:
            board: Board object

        Returns: None
        """
        raise NotImplementedError

    def serialize(self):
        """Used for UI.

        Returns: dictionary of this object
        """
        return {
            "action": self.__class__.__name__.lower(),
            "id": self.pawn.id,
            "color": self.pawn.owner.color == 1
        }


class Rotation(Action):
    """Used to rotate a pawn."""

    def __init__(self, pawn, delta):
        Action.__init__(self, pawn)
        self._from = pawn.rotation
        self._to = (pawn.rotation + delta) % 8

    def __str__(self):
        return f"{self.pawn} rotation {self._from} -> {self._to}  @{self.pawn.position}"

    def do(self, board):
        self.pawn.rotation = self._to
        self.pawn.owner.rotation_enabled = False
        return [self]

    def undo(self, board):
        self.pawn.rotation = self._from
        self.pawn.owner.rotation_enabled = True

    def serialize(self):
        res = super().serialize()
        res["from"] = self._from
        res["to"] = self._to
        return res


class Translation(Action):
    """Used to move a pawn."""
    def __init__(self, pawn, where):
        Action.__init__(self, pawn)
        self._from = pawn.position
        self._to = where
        if hasattr(self.pawn, 'first_move'):
            self._first_move = pawn.first_move

    def __str__(self):
        return f"{self.pawn} translation {self._from} -> {self._to}"

    def do(self, board):
        res = [self]

        # check opponent pawn at self._to
        dead_pawn = board.get_pawn_at(self._to)
        if dead_pawn is not None:
            if dead_pawn.owner == self.pawn.owner:
                logger.debug(board)
            assert dead_pawn.owner != self.pawn.owner
            death = Death(dead_pawn)
            res += death.do(board)

        # move
        board.set_pawn_at(self.pawn.position, None)
        self.pawn.position = self._to
        board.set_pawn_at(self.pawn.position, self.pawn)

        # rotation is now enabled
        self.pawn.owner.rotation_enabled = True
        return res

    def undo(self, board):
        board.set_pawn_at(self.pawn.position, None)
        self.pawn.position = self._from
        board.set_pawn_at(self.pawn.position, self.pawn)

        # for P0 and P1
        if hasattr(self.pawn, 'first_move'):
            if self._first_move:
                self.pawn.first_move = True

    def serialize(self):
        res = super().serialize()
        res["from"] = [(self._from % 9) - 1, int(self._from / 9)]
        res["to"] = [(self._to % 9) - 1, int(self._to / 9)]
        return res


class Death(Action):

    def __str__(self):
        return f"{self.pawn} death @{self.pawn.position}"

    def do(self, board):
        res = [self]
        assert self.pawn is not None
        if self.pawn not in board.pawns:
            raise Exception(f"{self.pawn}@{self.pawn.position} not found")
        board.dead_pawns.append(self.pawn)
        board.pawns.remove(self.pawn)

        assert (len(board.pawns) + len(board.dead_pawns) == 32)
        return res

    def undo(self, board):
        assert self.pawn is not None
        board.add_pawn(self.pawn)
        board.dead_pawns.remove(self.pawn)
        assert (len(board.pawns) + len(board.dead_pawns) == 32)


if __name__ == '__main__':
    from model.player import Player
    player = Player(1)
    pawn = King([0, 0], player)
    death = Death(pawn)
    print(death)

