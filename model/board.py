import logging
from model.pawns import *
from model.player import Player
from model.actions import Translation, Rotation, Death

loggerBoard = logging.getLogger("Board")


class Board:
    """8x8 squares board.
    Position 2D <-> 1D with :
    border | board game
        0  | 1  2  3  4  5  6  7  8
        9  | 10 11 12 13 14 15 16 17
        18 | 19 20 21 22 23 24 25 26
        27 | ...
        ...
        63 | 64 65 66 67 68 69 70 71

    """
    # Pawns placement at the start of the game
    PLACING = {
        P0: [10, 13, 14, 17],
        P1: [11, 12, 15, 16],
        Tower: [1, 8],
        Horseman: [2, 7],
        Mad: [3, 6],
        Lady: [4],
        King: [5]
    }

    def __init__(self):
        self.pawns = []
        self.dead_pawns = []
        self.history = []                               # actions historic
        self.shape = 8                                  # board dimension
        self.length = self.shape * (self.shape + 1)     # 1D grid dimension

        # for performances pawns are placed in this list:
        self.grid = [None for p in range(self.length)]

    def __str__(self):
        res = "  ." + "".join([f"  {chr(65 + i)}  ." for i in range(self.shape)]) + "\n"
        line = ""

        for tmp in range(self.length):
            x = tmp % (self.shape + 1) - 1
            y = int(tmp / (self.shape + 1)) + 1
            if x == 0:
                line = f"{y} |"
            p = self.grid[tmp]
            if p is None:
                line += "     |"
            else:
                line += f"{p}|"
            if x == self.shape - 1:
                res += line + "\n"

        return res

    # region Getters and setters

    def get_pawn_by_id(self, identifier):
        """
        Args:
            identifier: pawn identifier

        Returns: pawn if found, None if not
        """
        for pawn in self.pawns:
            if pawn.id == identifier:
                return pawn
        return None

    def get_king_of(self, owner):
        for pawn in self.pawns:
            if isinstance(pawn, King) and pawn.owner == owner:
                return pawn
        return None

    def get_pawn_at(self, pos: int) -> Pawn | None:
        """
        Args:
            pos: board position

        Returns: Pawn at the given position or None
        """
        return self.grid[pos]

    def set_pawn_at(self, pos: int, value):
        """
        Args:
            pos: 1D position
            value: pawn object

        Returns: None
        """
        self.grid[pos] = value

    # endregion

    def add_pawn(self, pawn: Pawn):
        """Place a pawn on the board (use pawn "position" argument).

        Args:
            pawn: pawn to place

        Returns: None
        """
        index = pawn.position
        self.grid[index] = pawn
        assert pawn not in self.pawns
        self.pawns.append(pawn)
        assert pawn.position < self.length

    def is_in_bound(self, pos: int):
        """Check if the given position is in the board.

        Args:
            pos: position to check

        Returns: True or False
        """
        down_bound = pos >= 0
        upper_bound = pos < self.length
        return down_bound and upper_bound and (pos % (self.shape + 1) != 0)

    def is_move_valid(self, pawn, pos: int):
        """Check if the given pawn can reach the given position.

        Args:
            pawn: the selected pawn
            pos:  the selected pawn destination
        :
        Returns: the move validity and destination content on board
        """
        if self.is_in_bound(pos):
            possible_pawn = self.get_pawn_at(pos)
            if possible_pawn is None:
                # free
                return True, possible_pawn
            else:
                # claimable
                if possible_pawn.owner != pawn.owner:
                    return True, possible_pawn
                # undercover
                else:
                    return False, possible_pawn
        return False, None

    def compute_all_translations(self):
        """For each pawn update moves.

        Returns: None
        """
        for pawn in self.pawns:
            pawn.reset()
        for pawn in self.pawns:
            pawn.compute_translations(self)

    def available_actions(self, player):
        """List available actions for a given player.

        Args:
            player: the given player object

        Returns: a list of actions (does not check king safety).
        """
        res = []
        for pawn in self.pawns:
            if pawn.owner == player:
                if pawn.translations_count > 0:
                    res += [Translation(pawn, where) for where in pawn.translations]
                if player.rotation_enabled:
                    res += [Rotation(pawn, delta) for delta in pawn.delta_rotations]
        return res

    # region High level
    def reset(self):
        """ Reset the board (for new game).

        Returns: None
        """
        self.pawns = []
        self.dead_pawns = []
        self.history = []

        # reset game's variables
        self.grid = [None for p in range(self.length)]

    def push(self, action):
        """Make an action and end the turn

        Args:
            action: action object to do.

        Returns: None
        """
        actions = action.do(self)
        self.history.append(actions)

        # compute new pawns translations and others indicators
        self.compute_all_translations()

    def pop(self):
        """Undo the last action made.

        Returns: None
        """
        res = []
        assert len(self.history) > 0  # for debug
        for action in self.history[-1]:
            action.undo(self)

        # update pawns translations and others indicators
        # self.compute_all_translations()

        # rm from historic
        res.append(self.history.pop())

        return res

    # endregion

    def check(self):
        """Check that the self.grid is up-to-date.

        Returns: True or False
        """
        occupied = []
        for pawn in self.pawns:
            if pawn.position not in occupied:
                occupied.append(pawn.position)
            else:
                loggerBoard.error(f"Multiple pawns at {pawn.position} {self.get_pawn_at(pawn.position)} !!!")
                return False

        success = True
        for i, pawn in enumerate(self.grid):
            if pawn is not None:
                if pawn not in self.pawns:
                    loggerBoard.error(f"Unexpected pawn at {i} {pawn} !!!")
                    success = False

                if pawn.position != i:
                    loggerBoard.warning(f"Position mismatch for {pawn}: {i} -> {pawn.position}")
                    success = False

        return success


