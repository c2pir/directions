import logging
import time


def timestamp(logger_name="TS"):
   def decorateur(function):
      def execution(*args, **kwargs):
          logger = logging.getLogger(logger_name)
          t0 = time.time()

          result = function(*args, **kwargs)

          logger.info(f"{logger_name} {function.__name__}, dt={time.time() - t0:.3f}s")
          return result

      return execution

   return decorateur
