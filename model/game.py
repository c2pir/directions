import logging
import numpy as np
import matplotlib.pyplot as plt

from model.board import Board
from model.decorators import timestamp
from model.pawns import King, Lady, Mad, Horseman, Tower, P0, P1
from model.player import Player

logger = logging.getLogger("Game")


class Indicators:
    def __init__(self, game):
        # basic (quality) value for each kind of pawn
        self.weighting = {
            P0: 4,
            P1: 3,
            Tower: 4*4,
            Horseman: 5,
            Mad: 4*6,
            Lady: 4*8,
            King: 151
        }
        self._game = game

    def weight_of(self, pawn):
        return self.weighting.get(pawn.__class__, 0)

    @property
    def scores(self):
        """Compute score for all players.

        Returns: computed scores.
        """
        result = [0, 0]
        for pawn in self._game.board.pawns:
            result[pawn.owner.index] += self.weight_of(pawn)
        return result

    def get_weights(self, alpha):
        result = np.ones((self._game.board.length,))
        for pawn in self._game.board.pawns:
            result[pawn.position] += alpha * self.weight_of(pawn)
        return result

    def get_occupation(self, player):
        occupation = np.zeros((self._game.board.length,))
        total = np.zeros_like(occupation)
        for pawn in self._game.board.pawns:
            total[pawn.translations] += 1.0
            total[pawn.position] += 2
            if pawn.owner == player:
                occupation[pawn.translations] += 1.0
                occupation[pawn.position] += 2
            else:
                occupation[pawn.translations] -= 1.0
                occupation[pawn.position] -= 2

        total += 1.0 * (total == 0)
        return occupation / total

    def board_values(self, player, alpha):
        o = self.get_occupation(player)
        o = o.reshape((8, 9))[:, 1:]
        w = self.get_weights(alpha).reshape((8, 9))[:, 1:]
        return o * w

    def notation_1(self, owner):
        res = 0
        for pawn in self._game.board.pawns:
            # potential strength
            potential = self._game.indicators.weight_of(pawn)
            n_pawn = 3 * potential

            # FIXME...
            n_pawn += 0.5 * potential * len(pawn.claimable)

            # FIXME...
            n_pawn += 0.1 * potential * len(pawn.under_cover)

            # add it to the notation
            if pawn.owner == owner:
                res += n_pawn
            else:
                res -= n_pawn

        return res

    def notation_2(self, owner, alpha=1.0):
        return self.board_values(owner, alpha).mean()

    def notation_3(self, owner, alpha=1.0):
        I = self.board_values(owner, alpha).mean()
        aI = np.abs(I.copy())
        pos = (aI.copy() + I.copy()).mean()
        neg = (aI.copy() - I.copy()).mean()
        if pos + neg != 0:
            return pos / (pos + neg)
        return 0


class Game:
    """
    Used to simulate a game.
    """
    NB_PLAYERS = 2

    def __init__(self):
        self.board = Board()

        # game info
        self.counter = 0
        self.turn = 0
        self.over = True        # is game over
        self.nb_players = 2
        self.players = [Player(i + 1) for i in range(self.NB_PLAYERS)]
        self.indicators = Indicators(self)

        self.historic = []

    def __str__(self):
        res = "Game state:\n"
        res += f"\tOver: {self.over}, "
        res += f"White is {'alive' if self.players[0].is_alive else 'dead'}, "
        res += f"Black is {'alive' if self.players[1].is_alive else 'dead'}\n"
        res += f"\tTurn {'white' if self.turn==0 else 'black'}\n"
        res += str(self.board)

        return res

    # region Getters and setters
    @property
    def actual_player(self):
        return self.players[self.turn]

    @property
    def actual_opponent_player(self):
        return self.players[(self.turn + 1) % 2]

    @property
    def available_actions(self):
        return self.board.available_actions(self.actual_player)

    @property
    def legal_actions(self):
        result = []
        legal_actions = self.board.available_actions(self.actual_player)
        for action in legal_actions:

            # do an available action
            self.board.push(action)

            if not self.checkmate(self.actual_player):
                result.append(action)

            # undo action
            self.board.pop()
        return result

    @property
    def winner(self):
        if self.players[0].is_alive and not self.players[1].is_alive:
            return self.players[0]
        elif self.players[1].is_alive and not self.players[0].is_alive:
            return self.players[1]
        return None

    @property
    def winner_index(self):
        if self.players[0].is_alive and not self.players[1].is_alive:
            return 0
        elif self.players[1].is_alive and not self.players[0].is_alive:
            return 1
        return None

    def set_player_brain(self, index: int, brain):
        """Set one player IA.

        Args:
            index: player inder (0 | 1)
            brain: IA object

        Returns: None
        """
        brain.owner = self.players[index]
        self.players[index].brain = brain

    def new(self):
        self.turn = 0
        self.counter = 0
        self.over = False
        self.historic = []

        self.board.reset()

        for player in self.players:
            player.reset()

        # pawns start placement
        for owner in self.players:
            for pawn_kind, positions in Board.PLACING.items():
                for position in positions:
                    pawn = pawn_kind(position, owner)
                    pos2d = pawn.position2d

                    # x symetry
                    if pos2d[1] == 0 and pos2d[0] > 4:
                        pawn.rotation = (8 - pawn.rotation) % 8

                    # extremities pawns
                    if pos2d[1] == 1 and (pos2d[0] == 0 or pos2d[0] == 7):
                        pawn.rotation = (pawn.rotation + 4) % 8

                    if owner.color != 1:
                        pawn.position = (self.board.shape + 1) * (self.board.shape - 1 - pos2d[1]) + pos2d[0] + 1
                        if hasattr(pawn, "first_move"):
                            pawn.first_move = True

                        pawn.rotation = (16 - (pawn.rotation + 4)) % 8
                    self.board.add_pawn(pawn)

        # check id's unicity
        ids = [p.id for p in self.board.pawns]
        assert len(ids) == len(set(ids))

        self.board.compute_all_translations()

    def push(self, action):
        self.board.push(action)

        self.turn = (self.turn + 1) % 2
        self.counter += 1

    def pop(self):
        result = self.board.pop()

        self.turn = (self.turn + 1) % 2
        self.counter -= 1
        return result

    def save_scores(self):
        for player, score in zip(self.players, self.indicators.scores):
            player.scores.append(score)

    def checkmate(self, player=None) -> bool:
        """Check for checkmate for the given player.
        Pawns translations must be computed before this call.

        Args:
            player: the given player object (if None the actual player).

        Returns: True if the given player's king is threatened
        """
        if player is None:
            player = self.actual_player

        king = self.board.get_king_of(player)
        if king is None:
            return True

        for pawn in self.board.pawns:
            if pawn.owner != player:
                if king.position in pawn.claimable:
                    return True
        return False

    @timestamp()
    def run(self, max_nb_turns=300):
        self.new()
        self.historic = [str(self.board)]

        counter = 0
        for counter in range(max_nb_turns):
            action = self.actual_player.brain.best_action(self)
            if action is None:
                self.over = True
                if self.checkmate():
                    self.actual_player.is_alive = False
                else:
                    logger.info(f"Pat for {self.actual_player}")
                break

            self.push(action)
            self.save_scores()

            self.historic.append(str(self))
            logger.debug(action)
            logger.debug("\n" + self.historic[-1])

        logger.debug(f"Number of turns: {counter}, winner: {self.winner}")
        return counter

    def plot_scores(self):
        plt.figure("Scores")
        plt.plot(self.players[0].scores, label=self.players[0].brain.__class__.__name__)
        plt.plot(self.players[1].scores, label=self.players[1].brain.__class__.__name__)
        plt.legend()
        plt.grid()
        plt.xlabel("turn")
        plt.ylabel("score")
        plt.show()


if __name__ == '__main__':
    from ia.alea import God
    from ia.recursion import IADepth

    logging.basicConfig(format="%(levelname)s:%(filename)s:%(lineno)d %(message)s")
    logger.setLevel(logging.DEBUG)
    logging.getLogger("TS").setLevel(logging.INFO)
    logging.getLogger("Brain").setLevel(logging.DEBUG)

    g = Game()
    g.set_player_brain(0, God())
    g.set_player_brain(1, IADepth(1))

    g.run()

    g.plot_scores()

    plt.figure("Occupation")
    o = g.indicators.get_occupation(g.actual_player)
    o = o.reshape((8, 9))
    w = g.indicators.get_weights(0.5).reshape((8, 9))
    plt.subplot(1, 2, 1)
    plt.imshow(o[:, 1:])
    plt.subplot(1, 2, 2)
    plt.imshow((w * o)[:, 1:])
    plt.show()
