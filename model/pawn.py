from model.player import Player


# FIXME
class Direction:
    def __init__(self, base_angle, translation_kind, distance):
        self.base_angle = base_angle
        self.kind = translation_kind
        self.distance = distance


class Pawn:

    def __init__(self, position, owner: Player, directions=None, rotation=0):
        if directions is None:
            directions = []
        self.id = id(self)      # unique pawn identifier
        self.owner = owner      # player that own this pawn

        # actual state
        self._position = position               # actual board position
        self.rotation = rotation                # actual rotation

        self._directions = [*directions]                 # absolute directions enabled
        self.delta_rotations = [i for i in range(1, 8)]  # Enabled delta (to avoid redundant actions)

        # computed positions
        self.free = []          # list of unoccupied positions reachable by self
        self.claimable = []     # list of enemies pawn positions threatened by self
        self.under_cover = []   # list of allies pawn positions protected by self

    def __str__(self):
        name = f"{self.__class__.__name__[:2]}:{self.rotation}"
        color = "w" if self.owner.color == 1 else "b"
        return f"{color}{name}"

    def reset(self):
        """Reset this pawn indicators

        Returns: None
        """
        self.free = []
        self.claimable = []
        self.under_cover = []

    @property
    def translations(self):
        result = [*self.free, *self.claimable]
        return result

    @property
    def directions(self):
        """Available directions for this pawn taking into account actual pawn rotation.

        Returns: available directions for this pawn.
        """
        return [(x + self.rotation) % 8 for x in self._directions]

    @property
    def translations_count(self):
        return len(self.translations)

    @property
    def position(self) -> int:
        """1D pawn position.

        Returns: actual position of this pawn
        """
        return self._position

    @position.setter
    def position(self, new_position):
        self._position = new_position

    @property
    def position2d(self) -> list:
        """Convert position to 2D (for display).

        Returns: actual position of this pawn [x, y]
        """
        return [self._position % (8 + 1) - 1, int(self._position / (8 + 1))]

    @property
    def translations2d(self) -> list:
        return [[t % (8 + 1) - 1, int(t / (8 + 1))] for t in self.translations]

    def _store_translation(self, position, validity, pawn_at_position):
        """

        Args:
            position: 1D position
            validity: position validity (see Board.is_move_valid)
            pawn_at_position: pawn at the given position, None if there is no pawn.

        Returns: None
        """
        if validity:
            if pawn_at_position is not None:
                self.claimable.append(position)
            else:
                self.free.append(position)
        else:
            if pawn_at_position is not None:
                # FIXME if "King" not in str(type(pawn_at_pos)) ?
                self.under_cover.append(position)

    def _line_translations(self, board):
        for direction in self.directions:
            for i in range(1, board.shape):
                pos = self.get(direction, i)
                valid, pawn_at_pos = board.is_move_valid(self, pos)
                self._store_translation(pos, valid, pawn_at_pos)
                if (pawn_at_pos is not None) or not valid:
                    break

    def compute_translations(self, board):
        """Compute allowed translations for this pawn.

        Args:
            board: board object containing this pawn.

        Returns: None
        """
        raise NotImplementedError

    def get(self, direction, distance):
        """Get the absolute position for a given direction at a given distance.
        See Board documentation.

        Deltas 1D position:
        8   9   10
        -1  0   1
        -10 -9  -8

        Args:
            direction: given direction (0: up, 2: right, 4: down, 6: left, ...: diagonals)
            distance: given number of squares of distance

        Returns: the 1D position
        """
        position = self._position
        if direction == 0:    # up
            position += 9 * distance
        elif direction == 1:
            position += 10 * distance
        elif direction == 2:  # right
            position += 1 * distance
        elif direction == 3:
            position += -8 * distance
        elif direction == 4:  # down
            position += -9 * distance
        elif direction == 5:
            position += -10 * distance
        elif direction == 6:  # left
            position += -1 * distance
        elif direction == 7:
            position += 8 * distance
        return position

    def rotate(self, delta):
        """Rotate this pawn

        Args:
            delta: new angle = (angle + delta) % 8

        Returns: None
        """
        self.rotation = (self.rotation + delta) % 8
