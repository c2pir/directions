"""
All pawns kinds classes.

"""
from model.pawn import Pawn


class P0(Pawn):
    def __init__(self, position, owner, rotation=4, **kwargs):
        Pawn.__init__(self, position, owner, [7, 0, 1, 4], rotation)

        # this kind of pawn can move 2 squares for it first move
        self.first_move = True

    @Pawn.position.setter
    def position(self, new_position):
        self._position = new_position
        self.first_move = False

    def compute_translations(self, board):
        for direction in self.directions:
            pos = self.get(direction, 1)
            valid, pawn_at_pos = board.is_move_valid(self, pos)
            self._store_translation(pos, valid, pawn_at_pos)
            if (pawn_at_pos is not None) or not valid:
                continue

            if self.first_move:
                pos = self.get(direction, 2)
                valid, pawn_at_pos = board.is_move_valid(self, pos)
                if valid and (pawn_at_pos is None):
                    self.free.append(pos)


class P1(Pawn):
    def __init__(self, position, owner, rotation=0, **kwargs):
        Pawn.__init__(self, position, owner, [7, 0, 1], rotation)
        self.first_move = True

    @Pawn.position.setter
    def position(self, new_position):
        self._position = new_position
        self.first_move = False

    def compute_translations(self, board):
        for direction in self.directions:
            pos = self.get(direction, 1)
            valid, pawn_at_pos = board.is_move_valid(self, pos)
            self._store_translation(pos, valid, pawn_at_pos)
            if (pawn_at_pos is not None) or not valid:
                continue

            if self.first_move:
                pos = self.get(direction, 2)
                valid, pawn_at_pos = board.is_move_valid(self, pos)
                if valid and (pawn_at_pos is None):
                    self.free.append(pos)


class Tower(Pawn):
    def __init__(self, position, owner, rotation=0, **kwargs):
        Pawn.__init__(self, position, owner, [0, 2, 4, 6], rotation)
        self.delta_rotations = [1]

    def compute_translations(self, board):
        self._line_translations(board)


class Horseman(Pawn):
    def __init__(self, position, owner, rotation=2, **kwargs):
        Pawn.__init__(self, position, owner, [1, 2, 6, 7], rotation)

    def compute_translations(self, board):
        for direction in self.directions:
            pos = self.get(direction, 1)
            valid = board.is_in_bound(pos)
            if valid:
                pos = self.get(direction, 2)
                valid, pawn_at_pos = board.is_move_valid(self, pos)
                self._store_translation(pos, valid, pawn_at_pos)

        # coup de sabot
        pos = self.get((4 + self.rotation) % 8, 1)
        valid, pawn_at_pos = board.is_move_valid(self, pos)
        self._store_translation(pos, valid, pawn_at_pos)


class Mad(Pawn):
    def __init__(self, position, owner, rotation=0, **kwargs):
        Pawn.__init__(self, position, owner, [1, 2, 3, 5, 6, 7], rotation)
        self.delta_rotations = [1, 2, 3, 4]

    def compute_translations(self, board):
        self._line_translations(board)


class Lady(Pawn):
    def __init__(self, position, owner, rotation=0, **kwargs):
        Pawn.__init__(self, position, owner, [0, 1, 2, 3, 4, 5, 6, 7], rotation)
        self.delta_rotations = []

    def compute_translations(self, board):
        self._line_translations(board)


class King(Pawn):
    def __init__(self, position, owner, **kwargs):
        Pawn.__init__(self, position, owner, [0, 1, 2, 3, 4, 5, 6, 7])
        self.delta_rotations = [1]

    def compute_translations(self, board):
        for direction in self.directions:
            pos = self.get(direction, 1)
            valid, pawn_at_pos = board.is_move_valid(self, pos)
            self._store_translation(pos, valid, pawn_at_pos)
