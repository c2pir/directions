class Player:
    """
    Player's data
    """

    def __init__(self, index):
        self.color = index            # 1: white, 2: black
        self.rotation_enabled = True  # Can the player do a rotation
        self.is_alive = True
        self.brain = None             # IA used
        self.scores = []              # Player score historic

    @property
    def index(self):
        return self.color - 1

    def __str__(self):
        BLUE = '\033[94m'
        RED = '\033[91m'
        ENDC = '\033[0m'
        res = "Human"
        if self.brain is not None:
            res = self.brain.__class__.__name__
        color = BLUE if self.color == 1 else RED
        return f"{color}{res}:{self.color}{ENDC}"

    def reset(self):
        """Reset player's data

        Returns: None
        """
        self.is_alive = True
        self.scores = []
        self.rotation_enabled = True
