import logging
import numpy as np
from trainning.versus import Versus

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from ia.alea import God
    from ia.recursion import IADepth

    logging.basicConfig(format="%(levelname)s:%(filename)s:%(lineno)d %(message)s")
    logging.getLogger("Versus").setLevel(logging.INFO)
    logging.getLogger("TS").setLevel(logging.INFO)

    alphas = np.linspace(0, 10, 30)  # [0.05, 0.1, 0.2, 0.3, 0.4, 0.8, 0.9, 1.0, 1.5, 1.9, 2, 2.5]

    brain1, brain2 = God(), IADepth(0)
    vs = Versus(brain1, brain2, 100)

    ratios = []
    for alpha in alphas:
        print(alpha)
        brain2.alpha = alpha
        vs.run()
        ratios.append(vs.win_rates)
    ratios = np.array(ratios)

    plt.title("IA wins ratio")
    plt.scatter(alphas, 100 * ratios[:, 1], label="ratio %")
    plt.xlabel("alpha")
    plt.legend()
    plt.grid()

    plt.show()
