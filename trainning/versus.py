import logging
import numpy as np
from model.decorators import timestamp
from model.game import Game

logger = logging.getLogger("Versus")


class Versus:
    """
    Used to simulate a game.
    """
    def __init__(self, brain1, brain2, iterations=5):
        self.game = Game()
        self.brain1 = brain1
        self.brain2 = brain2

        self._wins = [0, 0]
        self._nb_turns = []
        self._iterations = iterations

    @property
    def win_rates(self):
        return np.array(self._wins) / len(self._nb_turns)

    @property
    def durations(self):
        return self._nb_turns

    @property
    def mean_duration(self):
        return np.mean(self._nb_turns)

    @timestamp()
    def run(self, reset=True, max_nb_turns=200, after_game_over=None):
        if reset:
            self._wins = [0, 0]
            self._nb_turns = []

        logger.debug("brain1 as white player")
        self.game.set_player_brain(0, self.brain1)
        self.game.set_player_brain(1, self.brain2)

        for i in range(self._iterations):
            nb_turns = self.game.run(max_nb_turns=max_nb_turns)
            self._nb_turns.append(nb_turns)
            if self.game.winner_index is not None:
                self._wins[self.game.winner_index] += 1
                if after_game_over is not None:
                    after_game_over(self.game, len(self._nb_turns))

        logger.debug("brain1 as black player")
        self.game.set_player_brain(1, self.brain1)
        self.game.set_player_brain(0, self.brain2)

        for i in range(self._iterations):
            nb_turns = self.game.run(max_nb_turns=max_nb_turns)
            self._nb_turns.append(nb_turns)
            if self.game.winner_index is not None:
                self._wins[1 - self.game.winner_index] += 1
                if after_game_over is not None:
                    after_game_over(self.game, len(self._nb_turns))

        logger.debug(f"Win rates: {self.win_rates}, mean duration: {self.mean_duration}")


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from ia.alea import God
    from ia.recursion import IADepth

    logging.basicConfig(format="%(levelname)s:%(filename)s:%(lineno)d %(message)s")
    logger.setLevel(logging.DEBUG)
    logging.getLogger("TS").setLevel(logging.INFO)

    def save_game(game, index):
        f = open(f"data/{index}_previous.state", "w")
        f.write(str(game.historic[-2]))
        f.close()
        f = open(f"data/{index}_over.state", "w")
        f.write(str(game))
        f.close()

    vs = Versus(God(), IADepth(0), 4)

    ratios = []
    vs.run(after_game_over=save_game)
    ratios.append(vs.win_rates)
    for i in range(200):
        print(i)
        vs.run(reset=False, after_game_over=save_game)
        ratios.append(vs.win_rates)
    ratios = np.array(ratios)

    plt.subplot(2, 1, 1)
    plt.title("IA wins ratio")
    plt.plot(ratios[:, 0], label="God")
    plt.plot(ratios[:, 1], label="Depth")
    plt.legend()
    plt.grid()

    plt.subplot(2, 1, 2)
    plt.hist(vs.durations)

    plt.show()

