import logging
import pprint
from flask import Flask, jsonify  # request, make_response, render_template, render_template_string
from serializer import Direction

# Set loggings levels
logging.basicConfig(format="%(levelname)s:%(filename)s:%(lineno)d %(message)s")
log_flask = logging.getLogger('werkzeug')
loggerS = logging.getLogger("Instances")
loggerBoard = logging.getLogger("Board")
loggerB = logging.getLogger("Brain")
log_flask.setLevel(logging.ERROR)
loggerS.setLevel(logging.INFO)
loggerBoard.setLevel(logging.INFO)
loggerB.setLevel(logging.INFO)

# create server app
app = Flask(__name__)


@app.route("/direction", methods=['POST', 'GET'])
def direction_index():
    return app.send_static_file("direction/index.html")


@app.route("/direction/instances", methods=['POST'])
def direction_instances():
    return Direction.GetInstances()


@app.route("/direction/connect", methods=['POST'])
def direction_connect():
    return Direction.Connect()


@app.route("/direction/new_game", methods=['POST'])
def direction_new_game():
    return Direction.NewGame()


@app.route("/direction/user_input", methods=['POST'])
def direction_event():
    return Direction.UserInput()


@app.route("/direction/turn", methods=['POST'])
def direction_turn():
    return Direction.Turn()


@app.route("/direction/ping", methods=['POST'])
def direction_ping():
    return Direction.Ping()


if __name__ == '__main__':
    app.run(host="127.0.0.1", port=5000)
