import logging
import time

from model.actions import Translation, Rotation
from ia.recursion import IADepth
from ia.alea import God
from ia.genetic import IAGen
from flask import request, jsonify
from model.game import Game

loggerS = logging.getLogger("Instances")


class Instance:
    MAX_NB_USERS = 2
    LIFE_TIME = 3600*4  # max instance lifetime in seconds

    def __init__(self, user_name="undefined", debug_mode=0, creator_color="blanc", opponent_kind="human", **kwargs):
        self._start_date = time.time()
        self.game = Game()

        self.users = [user_name]
        self.debug_mode = debug_mode != 0

        if not self.debug_mode:
            self.creator_index = int(creator_color)
            self.opponent_kind = opponent_kind
            if self.opponent_kind != "human":
                self.users.append(opponent_kind)
            self.define_player_kind(self.creator_index, "human")
            self.define_player_kind((self.creator_index % 2) + 1, opponent_kind)

        self.phase = 0
        self.selection = None
        self.queue = []         # data that needs to be received by client(s)

    @property
    def old(self):
        return time.time() - self._start_date > Instance.LIFE_TIME

    @property
    def hide_new_game(self):
        if self.debug_mode:
            return False
        return self.opponent_kind == "human"

    @property
    def white_user_name(self):
        if self.debug_mode:
            return None
        if self.creator_index == 1:
            return self.users[0]
        else:
            if len(self.users) > 1:
                return self.users[1]
        return None

    @property
    def black_user_name(self):
        if self.debug_mode:
            return None
        if self.creator_index == 1:
            if len(self.users) > 1:
                return self.users[1]
        else:
            return self.users[0]
        return None

    @property
    def actual_user_name(self):
        """
        :return: Actual username if it is human
        """
        if self.debug_mode:
            return None
        if self.game.actual_player.color == self.creator_index:
            return self.users[0]
        elif self.opponent_kind == "human":
            return self.users[1]
        return None

    @property
    def actual_opponent_user_name(self):
        """
        :return: Actual opponent username if it is human
        """
        if self.debug_mode:
            return None
        if self.game.actual_player.color != self.creator_index:
            return self.users[0]
        if self.game.actual_player.color == self.creator_index:
            if self.opponent_kind == "human":
                return self.users[1]
            return None

    def define_player_kind(self, index, kind):
        player = self.game.players[index - 1]
        if kind == "human":
            player.brain = None
        elif kind == "alea":
            self.game.set_player_brain(index - 1, God())
        elif kind == "ia1":
            self.game.set_player_brain(index - 1, IADepth(0))
        elif kind == "ia2":
            self.game.set_player_brain(index - 1, IADepth(1))
        elif kind == "genetic":
            brain = IAGen()
            brain.load("../best.adn")
            self.game.set_player_brain(index - 1, brain)

    def add_user(self, user_name):
        if user_name not in self.users:
            if len(self.users) >= self.MAX_NB_USERS or self.debug_mode:
                return False
            self.users.append(user_name)

            # all user are connected start the game
            _ = self.start_new_game()

        if self.game is not None:
            # load or reload game for this user
            self.queue.append({
                "user_name": user_name,
                "data": self.serialize_actual_state()
            })
        return True

    def add_to_queue(self, user_name, data):
        self.queue.append({
            "user_name": user_name,
            "data": data
        })

    def start_new_game(self):
        """
        :return: server answer
        """
        self.game.new()

        server_answer = self.serialize_actual_state()
        if not self.debug_mode:
            self.add_to_queue(self.users[0], server_answer)
        if self.game.actual_player.brain is not None:
            self.add_to_queue(self.users[0], self.start_new_turn())

        return server_answer

    def _ending_turn(self):
        for x in self.game.board.history[-1]:
            loggerS.info(x)

        self.game.save_scores()

        self.phase = 0
        data_end_turn = {
            "action": "end turn",
            "args": {
                "turn": self.game.turn,
                "counter": self.game.counter,
                "turns": [i for i in range(len(self.game.players[0].scores))],
                "white": self.game.players[0].scores,
                "black": self.game.players[1].scores
            }
        }
        if self.debug_mode:
            self.add_to_queue(self.users[0], data_end_turn)
            return

        self.add_to_queue(self.actual_user_name, data_end_turn)
        if self.actual_opponent_user_name is not None:
            self.add_to_queue(self.actual_opponent_user_name, data_end_turn)

    def start_new_turn(self):
        """
        :return:
        """
        server_answer = {}

        if self.game.over:
            return server_answer

        # IA
        if self.game.actual_player.brain is not None:
            best_action = self.game.actual_player.brain.best_action(self.game)
            if best_action is not None:
                self.game.push(best_action)
                server_answer = [x.serialize() for x in self.game.board.history[-1]]
            else:
                self.game.over = True
                if self.game.checkmate():
                    self.game.actual_player.is_alive = False
                server_answer = {
                    "action": "over",
                    "id": self.game.actual_player.color,
                    "color": self.game.actual_player.color == 1
                }
            self._ending_turn()

        else:
            if len(self.game.legal_actions) == 0:
                self.game.actual_player.is_alive = False
                server_answer = {
                    "action": "over",
                    "id": self.game.actual_player.color,
                    "color": self.game.actual_player.color == 1
                }

        return server_answer

    def serialize_actual_state(self):
        """
        :return: the serialized game.
        """
        res = []
        for pawn in self.game.board.pawns:
            dp = {
                "x": int(pawn.position2d[0]),
                "y": int(pawn.position2d[1]),
                "rotation": pawn.rotation,
                "src": f"static/direction/images/{pawn.__class__.__name__.lower()}.png",
                "color": pawn.owner.color == 1,
                "id": pawn.id
            }
            res.append(dp)
        return {
            "action": "load",
            "nb_turns": self.game.counter,
            "actual_player_index": self.game.actual_player.index,
            "pawns": res,
            "white_user_name": self.white_user_name,
            "black_user_name": self.black_user_name
        }

    def _pawn_click(self, event):
        res = []
        if self.phase == 0:
            self.game.board.compute_all_translations()
            self.selection = self.game.board.get_pawn_by_id(event["args"][0])
            if self.selection.owner == self.game.actual_player:
                res.append({
                    "action": "select",
                    "args": {
                        "id": self.selection.id,
                        "color": self.selection.owner.color == 1,
                        "rotation": self.selection.rotation,
                        "src": f"static/direction/images/{self.selection.__class__.__name__.lower()}.png",
                        "rotation_enabled": self.game.actual_player.rotation_enabled,
                        "moves": [[int(y) for y in x] for x in self.selection.translations2d]
                    }
                })
                self.phase = 1

        elif self.phase == 1:
            res = [{
                "action": "unselect",
                "args": {
                    "id": self.selection.id,
                    "color": self.selection.owner.color == 1,
                    "rotation": self.selection.rotation,
                    "src": f"static/direction/images/{self.selection.__class__.__name__.lower()}.png"
                }
            }]
            selection = self.game.board.get_pawn_by_id(event["args"][0])

            # check it is a legal move
            if selection.position in self.selection.translations:
                # eat
                action = Translation(self.selection, selection.position)
                self.game.push(action)
                res = [x.serialize() for x in self.game.board.history[-1]]

                # send info to other human player
                if self.actual_user_name is not None:
                    for x in res:
                        self.add_to_queue(self.actual_user_name, x)
                self.phase = 2

            elif selection.owner != self.game.actual_player:
                self.phase = 0

            else:
                self.selection = selection
                self.phase = 1
                select_data = {
                    "user_name": self.actual_user_name,
                    "data": {
                        "action": "select",
                        "args": {
                            "id": selection.id,
                            "color": selection.owner.color == 1,
                            "rotation": selection.rotation,
                            "src": f"static/direction/images/{selection.__class__.__name__.lower()}.png",
                            "rotation_enabled": self.game.actual_player.rotation_enabled,
                            "moves": [[int(y) for y in x] for x in selection.translations2d]
                        }
                    }
                }
                self.queue.append(select_data)
        return res

    def _square_click(self, event):
        res = []
        if self.phase == 1:
            # check it is a legal move
            absolute_position = event["args"][0] + 1 + (self.game.board.shape + 1) * event["args"][1]
            if absolute_position in self.selection.translations:
                action = Translation(self.selection, absolute_position)
                self.game.push(action)
                res = [x.serialize() for x in self.game.board.history[-1]]

                # send info to other human player
                if self.actual_user_name is not None:
                    for x in res:
                        self.add_to_queue(self.actual_user_name, x)
                self.phase = 2
            else:
                self.phase = 0

        return res

    def _rotation_click(self, event):
        action = Rotation(self.selection, event["args"][0])
        self.game.push(action)
        res = [x.serialize() for x in self.game.board.history[-1]]

        # send info to other human player
        if self.actual_user_name is not None:
            for x in res:
                self.add_to_queue(self.actual_user_name, x)
        self.phase = 2
        return res

    def on_user_input(self, event):
        loggerS.debug(event)
        res = {}

        if self.debug_mode:
            if event["what"] == "owner kind":
                self.define_player_kind(event["args"][0], event["args"][1])
                return res

        if self.game.actual_player.brain is None:
            if (not self.debug_mode) and (event["user_name"] != self.actual_user_name):
                return res

            if event["what"] == "pawn":
                res = self._pawn_click(event)

            elif event["what"] == "square":
                res = self._square_click(event)

            elif event["what"] == "radio direction":
                res = self._rotation_click(event)

            # check if turn is finished
            if self.phase == 2:
                self._ending_turn()

        loggerS.info(res)
        return res

    def ping_updates(self, user_name):
        res = []
        for update in self.queue:
            if update["user_name"] == user_name or self.debug_mode:
                res.append(update["data"])
                self.queue.remove(update)
        if len(res) != 0:
            loggerS.debug(f"{user_name}, {res}")
        return res


class Direction:
    _instances = {}

    @classmethod
    def CleanInstances(cls):
        to_remove = []
        for name in cls._instances:
            if cls._instances[name].old:
                to_remove.append(name)
        for name in to_remove:
            del cls._instances[name]

    @classmethod
    def Instances(cls):
        return [name for name in cls._instances]

    @classmethod
    def GetInstances(cls):
        res = {}

        # automatic instances clean
        cls.CleanInstances()

        try:
            instances = cls.Instances()
            if len(instances) != 0:
                res["instances"] = instances
            return jsonify(res)
        except Exception as e:
            loggerS.error(str(e))
        return res

    @classmethod
    def Connect(cls):
        server_answer = {"status": 0}
        try:
            text = request.data.decode()
            json = eval(text)
            game_name = json["game_name"]
            user_name = json["user_name"]
            if len(cls.Instances()) >= 10:
                # TODO
                return {"status": -1, "msg": "Trop de parties en cours pour en créer une nouvelle."}

            if game_name not in cls._instances:
                cls._instances[game_name] = Instance(**json)

            if not cls._instances[game_name].add_user(user_name):
                return {"status": -1, "msg": f"La partie est pleine.\n{cls._instances[game_name].users}"}

            server_answer["debug_mode"] = cls._instances[game_name].debug_mode
            server_answer["hide_new_game"] = cls._instances[game_name].hide_new_game
        except Exception as e:
            server_answer["status"] = -10
            loggerS.error(str(e))
        return jsonify(server_answer)

    @classmethod
    def NewGame(cls):
        try:
            text = request.data.decode()
            json = eval(text)
            game_name = json["game_name"]
            if game_name in cls._instances:
                return jsonify(cls._instances[game_name].start_new_game())
        except Exception as e:
            loggerS.error(str(e))
        return {}

    @classmethod
    def UserInput(cls):
        try:
            text = request.data.decode()
            json = eval(text)
            game_name = json["game_name"]
            if game_name in cls._instances:
                return jsonify(cls._instances[game_name].on_user_input(json))
        except Exception as e:
            loggerS.error(str(e))
        return jsonify({})

    @classmethod
    def Turn(cls):
        text = request.data.decode()
        json = eval(text)
        game_name = json["game_name"]
        if game_name in cls._instances:
            if "start" in json["what"]:
                return jsonify(cls._instances[game_name].start_new_turn())
        return jsonify({})

    @classmethod
    def Ping(cls):
        try:
            text = request.data.decode()
            json = eval(text)
            game_name = json["game_name"]
            user_name = json["user_name"]
            if game_name in cls._instances:
                return jsonify(cls._instances[game_name].ping_updates(user_name))
        except Exception as e:
            loggerS.error(str(e))
        return jsonify({})


