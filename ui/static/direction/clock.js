/*
    Ping
*/
var dt;
var last_event_time;

function timerCallback() {
    if (this.status != 200) {console.log("COM ERROR!")}
	else {
	    server_answer = JSON.parse(this.responseText);
	    if (!Array.isArray(server_answer)) return;
	    server_answer.forEach(
	        (data) => {
	            dispatcher(data);
	        }
	    )
	}
}

function runTimer()
{
    var date = new Date();
    var ms = date - last_event_time;
    var remaining_time = dt - ms/1000.0; // each dt in second

    if (remaining_time<=0) {
        var oReq = new XMLHttpRequest();
        oReq.open("POST", SERVER_ADDRESS + "direction/ping", true);
        oReq.addEventListener('load', timerCallback);
        oReq.send(JSON.stringify({"game_name": GAME_NAME, "user_name": USER_NAME}));

        last_event_time = new Date();
    }

    window.setTimeout(() => runTimer(), Math.round(1000*dt));
}
function startTimer(delta) {
    dt = delta;
    last_event_time = new Date();
    runTimer();
}