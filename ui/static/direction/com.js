
function send_post(address, data, callback) {
    data["game_name"] = GAME_NAME;
    data["user_name"] = USER_NAME;

    // example python flask
	var oReq = new XMLHttpRequest();
	oReq.open("POST", SERVER_ADDRESS + "direction/" + address, true);
	oReq.addEventListener('load', callback);

	oReq.send(JSON.stringify(data));
}


function send_get(address) {
    // example python flask
	var oReq = new XMLHttpRequest();
	oReq.open("GET", SERVER_ADDRESS + "/direction" + address);
	oReq.send(null);
}