/*
    connexion window
*/

function openTab(evt, tabName) {
    SELECTED_TAB = tabName;

    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}

function instancesCallback() {
    if (this.status != 200) {console.log("COM ERROR!")}
	else {
	    server_answer = JSON.parse(this.responseText);
	    if (server_answer.instances != undefined)
	    {
            var listI = document.getElementById("instances_list");
            listI.innerHTML = "";

            server_answer.instances.forEach(
                (name) => {
                    var div = document.createElement("div");
                    div.className = "instance_item";

                    var label = document.createElement("label")
                    //label.for = "selected_instance";
                    label.innerHTML = name;

                    var radio = document.createElement("input")
                    radio.type = "radio";
                    radio.name = "selected_instance";
                    radio.value = name;

                    div.appendChild(radio);
                    div.appendChild(label);

                    listI.appendChild(div);
                }
            );
	    }
	    else {
	        // TODO
	    }
	}
}

function connectionCallback() {
    if (this.status != 200) {console.log("COM ERROR!")}
	else {
	    server_answer = JSON.parse(this.responseText);
	    console.log(server_answer)
	    if (server_answer.status == 0)
	    {
            if (server_answer.debug_mode) {
                document.getElementById('jn_select').style.display="inline-block";
                document.getElementById('jb_select').style.display="inline-block";
            }
            else {
                document.getElementById('jn_select').style.display="none";
                document.getElementById('jb_select').style.display="none";
            }
            if (server_answer.hide_new_game) {
                document.getElementById('new_game_button').style.visibility="collapse";
            }
            else {
                document.getElementById('new_game_button').style.visibility="visible";
            }
            document.getElementById("connection").style = "visibility: hidden;";

            startTimer(0.1);
	    }
	    else {
	        alert(server_answer.msg);
	    }
	}
}

function connectionClick() {
    if (SELECTED_TAB == "create_instance") {
        GAME_NAME = document.getElementById("game_name_field").value;
    }
    else {
        var radio = document.querySelector('input[name="selected_instance"]:checked');
        if (radio != undefined) {
            GAME_NAME = radio.value;
        }
        else {
            alert("Veuillez séléctionner une partie.");
            return;
        }
    }

    USER_NAME = document.getElementById("user_name_field").value;

    var debug_mode = 0;
    if (document.getElementById("debug_mode_field").checked) {debug_mode=1;}
    var creator_color = document.getElementById("color_select").value;
    var opponent_kind = document.getElementById("opponent_select").value;
    send_post("connect", {"debug_mode": debug_mode, "creator_color": creator_color, "opponent_kind": opponent_kind}, connectionCallback);
}