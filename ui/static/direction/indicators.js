
function update_curves(turns, black_scores, white_scores){
	var data = [
        {
            x: turns,
            y: black_scores,
            name: "Noir"
        },
        {
            x: turns,
            y: white_scores,
            name: "Blanc",
        }
	];
	var layout = {
        /*autosize: true,*/
        margin: { t: 20, b: 20 }
	};
	var config = {responsive: true}
	Plotly.newPlot( "curves_container", data, layout, config);
}