function onLoad() {
    send_post("instances", {}, instancesCallback);

    drawGridWithDiagonals();
    openNav();
    document.getElementById("jn_select").value = "human";
    document.getElementById("jb_select").value = "human";
}

function openNav() {
  document.getElementById("game_infos").style.width = "25vw";
}

function closeNav() {
  document.getElementById("game_infos").style.width = "0";
}

function closeGameOver(){
  document.getElementById("game_over").style = "display: none;";
}

// Fonction pour dessiner une grille 8x8 avec diagonales de chaque carré sur un canvas
function drawGridWithDiagonals() {
  const canvas = document.getElementById('board_canvas');
  const ctx = canvas.getContext('2d');
  ctx.strokeStyle = "brown";

  const cellSize = canvas.width / 7;

  document.getElementById('squares_container').innerHTML = "";
  // Dessiner la grille
  for (let i = 0; i < 8; i++) {
    for (let j = 0; j < 8; j++) {
        if (i<7) {
            ctx.strokeRect(i * cellSize, j * cellSize, cellSize, cellSize);

            // Dessiner les diagonales de chaque carré
            ctx.beginPath();
            ctx.moveTo(i * cellSize, j * cellSize);
            ctx.lineTo((i + 1) * cellSize, (j + 1) * cellSize);
            ctx.moveTo((i + 1) * cellSize, j * cellSize);
            ctx.lineTo(i * cellSize, (j + 1) * cellSize);
            ctx.stroke();
        }

        create_square(i, j);
    }
  }

  //create_pawn({"x": 0, "y": 1, "src": "images/horseman.png", "a": 1, "color": false});
  //create_pawn({"x": 5, "y": 1, "src": "images/horseman.png", "a": 1, "color": true});
}
function updateTurn(turn) {
    if (turn == 0) {
        document.getElementById("player1").className = "player_container border";
        document.getElementById("player2").className = "player_container";
    }
    else {
        document.getElementById("player1").className = "player_container";
        document.getElementById("player2").className = "player_container border";
    }
}
function updatePawn(data, ele) {
    if (data.src != undefined) { ele.src = data.src; }
    x = 1.25 + data.x * 12.5;
    y = 1.25 + data.y * 12.5;
    a = 45 * data.rotation;
    var style = `left: ${x}%; bottom: ${y}%;`;
    style += `transform: rotateZ(${a}deg);`;
    if (data.color) style += "filter: invert();"
    ele.style = style;
}
function updatePawnColor(color, ele) {
    if (color) { ele.style.filter = "invert()"; }
    else { ele.style.filter = ""; }
}

/*
    Create HTML elements
*/
function create_square(i, j) {
    var ele = document.createElement("div");
    ele.className = "square_div";
    x = 3.75 + i * 12.5;
    y = 3.75 + j * 12.5;
    ele.id="s_"+i.toString()+"_"+j.toString();
    ele.style = `left: ${x}%; bottom: ${y}%;`;
    ele.onclick = Function(`squareClick(${i},${j})`)

    var container = document.getElementById('squares_container');
    container.appendChild(ele);
}
function create_pawn(data) {
    var ele = document.createElement("img")
    ele.id = data.id.toString();
    ele.className = "pawn_img";
    ele.onclick = Function(`pawnClick(${data.id})`)
    updatePawn(data, ele)

    var container = document.getElementById('pawns_container');
    container.appendChild(ele);
}

/*
    Callbacks
*/
function update_selection(moves) {
    //background: chartreuse;
    for (let i = 0; i < 8; i++) {
        for (let j = 0; j < 8; j++) {
            var id = "s_"+i.toString()+"_"+j.toString();
            if (JSON.stringify(moves).includes(JSON.stringify([i, j]))) document.getElementById(id).style.background = "chartreuse";
            else document.getElementById(id).style.background = "";
        }
    }
}
function dispatcher(data) {
    if (data.action != undefined) {console.log(data.action, data);}
    switch (data.action) {
        case "load":
            document.getElementById('pawns_container').innerHTML="";
            data.pawns.forEach((data) => create_pawn(data));
            document.getElementById("nb_turns").innerHTML = data.nb_turns;
            document.getElementById('jb_name').innerHTML = data.white_user_name;
            document.getElementById('jn_name').innerHTML = data.black_user_name;
            updateTurn(data.actual_player_index);
            update_selection([])
            break;
        case "select":
            var ele = document.getElementById(data.args.id.toString());
            if (data.args.color) {
                ele.style.filter = "invert() drop-shadow(0 0 0.75rem crimson)";
            }
            else {
                ele.style.filter = "drop-shadow(0 0 0.75rem crimson)";
            }

            // show selection
            update_selection(data.args.moves)

            var img = document.getElementById("rotation_img");
            img.src = data.args.src;
            var style = `transform: rotateZ(${45 * data.args.rotation}deg);`;
            if (data.args.color) style += "filter: invert() drop-shadow(gold 0px 0px 0.5rem);"
            else style += "filter: drop-shadow(gold 0px 0px 0.5rem);"
            img.style = style;
            if (data.args.rotation_enabled) {document.getElementById("rotation_container").style = "";}
            document.getElementById("start_rotation").checked = true;
            break;
        case "unselect":
            var ele = document.getElementById(data.args.id.toString());
            updatePawnColor(data.args.color, ele);
            document.getElementById("rotation_container").style = "visibility: collapse;";
            update_selection([])
            break;
        case "translation":
            var ele = document.getElementById(data.id.toString());
            ele.style.left = (1.25 + data.to[0] * 12.5).toString() + "%";
            ele.style.bottom = (1.25 + data.to[1] * 12.5).toString() + "%";
            updatePawnColor(data.color, ele);
            document.getElementById("rotation_container").style = "visibility: collapse;";
            break;
        case "rotation":
            var ele = document.getElementById(data.id.toString());
            var a = 45 * data.to;
            ele.style.transform = `rotateZ(${a}deg)`;
            updatePawnColor(data.color, ele);
            document.getElementById("rotation_container").style = "visibility: collapse;";
            break;
        case "death":
            var ele = document.getElementById(data.id.toString());
            ele.style = "display: none;";
            break;
        case "end turn":
            updateTurn(data.args.turn);
            document.getElementById("nb_turns").innerHTML = data.args.counter.toString();
            update_curves(data.args.turns, data.args.black, data.args.white);
            update_selection([])

            send_post("turn", { "what": "start" }, server_answer_callback);
            break;
        case "over":
            document.getElementById("game_over").style = "";
            // for plotly autoresize
            window.dispatchEvent(new Event('resize'));

            if (!data.color) {
                document.getElementById("winner_icon").style = "filter: invert() drop-shadow(0 0 0.75rem gold);";
            }
            else {
                document.getElementById("winner_icon").style = "filter: drop-shadow(0 0 0.75rem gold);";
            }

            update_selection([])
            break;
        default:
            break;
    }
}

function server_answer_callback() {
    if (this.status != 200) {console.log("COM ERROR!")}
	else {
	    server_answer = JSON.parse(this.responseText);
        //console.log(server_answer);
        if (!Array.isArray(server_answer)) {
            dispatcher(server_answer)
        }
        else {
            server_answer.forEach(
                (data) => {
                    dispatcher(data);
                }
            )
        }
        //
	}
}


/*
    Events
*/
function newGameClick() {
    var data = {"what": "owner kind", "args": [2, document.getElementById("jn_select").value]};
    send_post("user_input", data, server_answer_callback);
    data = {"what": "owner kind", "args": [1, document.getElementById("jb_select").value]};
    send_post("user_input", data, server_answer_callback);

    send_post("new_game", {}, server_answer_callback);
}
function squareClick(x, y) {
    //console.log(x, y);
    send_post("user_input", {"what": "square", "args": [x, y]}, server_answer_callback);
}
function pawnClick(id) {
    //console.log(id);
    send_post("user_input", {"what": "pawn", "args": [id]}, server_answer_callback);
}
function directionClick(angle) {
    //console.log(angle);
    send_post("user_input", {"what": "radio direction", "args": [angle]}, server_answer_callback);
}
function playerKindChange(index, id) {
    var data = {"what": "owner kind", "args": [index, document.getElementById(id).value]};
    //console.log(data)
    send_post("user_input", data, server_answer_callback);
}